// $("ul").carousel({
//     size             : 1,
//     speed            : 300,
//     visible          : 4,
//     visibletablet    : 2,
//     visiblemobile    : 1,
//     vertical         : false, //true or false
//     elscroll         : false, //tag or class element to scroll
//     scrolling        : true, //true or false (auto scrolling)
//     scrolltime       : 7000,
//     inversescroll    : false, //true or false
//     margin           : 10,  //margin for one side in px
//     prev             : true, //html code, text or (true/false) for button prev
//     next             : true, //html code, text or (true/false) for button next
//     prevclass        : "icon-arrow-left",
//     nextclass        : "icon-arrow-right"
// });
//
// Data attributes:
//     data-elscroll = "value"
//     data-visible = "value"
//     data-visibletablet = "value"
//     data-visiblemobile = "value"
//     data-size = "value"
//     data-margin = "value"
//     data-speed = "value"
//     data-vertical = "value"
//     data-scrolling = "value"
//     data-scrolltime = "value"
//     data-inversescroll = "value"
//     data-prev = "value"
//     data-next = "value"
//     data-prevclass = "value"
//     data-nextclass = "value"

(function($){
    $.fn.carousel = function(o){
        var options = $.extend({
            size          : 1,
            speed         : 200,
            visible       : 3,
            visibletablet : 2,
            visiblemobile : 1,
            vertical      : false,
            elscroll      : false,
            scrolling     : true,
            scrolltime    : 7000,
            inversescroll : false,
            margin        : 10,
            prev          : true,
            next          : true,
            prevclass     : false,
            nextclass     : false
        }, o || {});
        return this.each(function(){

            var $ul = $(this), data = $ul.data();
            var o = JSON.parse(JSON.stringify(options));
            o = $.extend(o, data);

            var $win = $(window), $div, $li = o.elscroll ? $(this).find(o.elscroll) : $ul.children(),
                liLength = $li.length, liH, liFullHeight,
                v = o.visible, vD = v, vT = (v > o.visibletablet ? o.visibletablet : v), vM = (v > o.visiblemobile ? o.visiblemobile : v),
                size = (o.size > v ? v : o.size), margin,
                speed = o.speed,
                vertical = o.vertical,
                scrolling = o.scrolling,
                inversescroll = o.inversescroll,
                scrolltime = o.scrolltime,
                button = true,
                prev = o.prev,
                next = o.next,
                prevclass = o.prevclass,
                nextclass = o.nextclass,
                autoScrolling, scrollingTimeout,
                desktop = true, tablet = true, mobile = true;

            if($ul.find('img').length && vertical && !o.img){
                var currLength = 0;
                $ul.find('img').each(function(){
                    var _img = $(this).get(0);
                    var newImg = new Image();
                    newImg.onload = function() {
                        currLength++;
                        if($ul.find('img').length === currLength){
                            $ul.carousel($.extend(o, {img : true}));
                        }
                    };
                    newImg.src = _img.src;
                });
                return;
            }

            if(!$ul.hasClass('carousel')){
                $ul.addClass('carousel');
            }

            var fun = {
                wrap       : function(){
                    //Code for the wrapper
                    fun.button();
                    $li.addClass('carousel-item').wrapAll('<div class="carousel-clip">' + '<div class="carousel-wrap"></div>' + '</div>');
                    $div = $('.carousel-wrap', $ul);
                },
                button     : function(){
                    if((prev && next) && (v < liLength) && button){
                        button = false;
                        $ul.append(
                                $('<a/>', {
                                    href          : "javascript:void(0);",
                                    title         : "Prev",
                                    onselectstart : "return false",
                                    unselectable  : "on",
                                    class         : "prev carousel-btn " + (prevclass ? prevclass : ''),
                                    html          : (typeof (prev) !== "boolean" ? prev : "")
                                })
                            )
                            .append(
                                $('<a/>', {
                                    href          : "javascript:void(0);",
                                    title         : "Next",
                                    onselectstart : "return false",
                                    unselectable  : "on",
                                    class         : "next carousel-btn " + (nextclass ? nextclass : ''),
                                    html          : (typeof (next) !== "boolean" ? next : "")
                                })
                            );
                    }
                },
                size       : function(){
                    size = (o.size > v ? v : o.size);
                    size = size > (liLength - v) ? liLength - v : size;
                },
                style      : function(){
                    margin = o.margin / $ul.width() * 100 / (liLength > v ? liLength / v : 1);
                    liH = $li.height();
                    liFullHeight = $li.outerHeight(true);
                    $li.each(function(){
                        if(liH < $(this).height()){
                            liH = $(this).height();
                        }
                        if(liFullHeight < $(this).outerHeight(true)){
                            liFullHeight = $(this).outerHeight(true);
                        }
                    });
                    $ul.find('.carousel-clip').css({
                        'height' : vertical ? liFullHeight * v : ''
                    });
                    $div.css({
                        'width'  : vertical ? '' : 100 * liLength / v + '%',
                        'height' : vertical ? liFullHeight * liLength : ''
                    });
                    $li.css({
                        'width'        : vertical ? '100%' : (liLength > v ? 100 / liLength : 100 / v) - (v !== 1 ? margin * 2 : 0) + '%',
                        'height'       : vertical ? liH : '',
                        'margin-left'  : vertical ? '0' : (v !== 1 ? margin : 0) + '%',
                        'margin-right' : vertical ? '0' : (v !== 1 ? margin : 0) + '%'
                    }).addClass('carousel-item');
                },
                btnClick   : function(direction){
                    clearInterval(autoScrolling);
                    clearTimeout(scrollingTimeout);
                    direction ? fun.next() : fun.prev();
                    if(scrolling){
                        scrollingTimeout = setTimeout(function(){
                            fun.autoScrolling();
                        }, speed);
                    }
                },
                prev       : function(){
                    $div.children(':gt(' + (liLength - size - 1) + ')').prependTo($div);
                    if(vertical){
                        $($div).stop(false, false).css({"margin-top" : -liFullHeight * size + 'px'}).animate({'marginTop' : "0"}, speed);
                    }else{
                        $($div).stop(false, false).css({"margin-left" : -100 * size / v + '%'}).animate({'marginLeft' : "0"}, speed);
                    }
                },
                next       : function(){
                    if(vertical){
                        $div.stop(true, true).animate({'marginTop' : -liFullHeight * size + 'px'}, speed, function(){
                            $div.children(':lt(' + size + ')').appendTo($div);
                            $div.css({"margin-top" : "0"});
                        });
                    }else{
                        $div.stop(true, true).animate({'marginLeft' : -100 * size / v + '%'}, speed, function(){
                            $div.children(':lt(' + size + ')').appendTo($div);
                            $div.css({"margin-left" : "0"});
                        });
                    }
                },
                device : function(){
                    if($win.width() >= 800 && desktop){
                        fun.responsive(false, true, true);
                    }else if($win.width() <= 800 && $win.width() >= 480 && tablet){
                        fun.responsive(true, false, true);
                    }else if($win.width() <= 480 && mobile){
                        fun.responsive(true, true, false);
                    }
                },
                responsive : function(d, t, m){
                    desktop = d, tablet = t, mobile = m;
                    if(!d){
                        v = vD;
                    }else if(!t){
                        v = vT;
                    }else if(!m){
                        v = vM;
                    }
                    fun.size();
                    fun.style();
                    clearInterval(autoScrolling);
                    if(v >= liLength){
                        $('.carousel-btn', $ul).remove();
                    }else{
                        fun.button();
                        fun.autoScrolling();
                    }
                },
                autoScrolling : function(){
                    var scrollingInversion = inversescroll ? fun.prev : fun.next;
                    autoScrolling = scrolling && (v < liLength) ? setInterval(scrollingInversion, scrolltime) : '';
                }
            };

            fun.wrap();
            fun.device();

            // Prev button
            $(this).on('click', '.prev', function(){
                fun.btnClick(false);
            });
            // Next button
            $(this).on('click', '.next', function(){
                fun.btnClick(true);
            });

            if(!vertical){
                $win.resize(function(){
                    fun.device();
                });
            }

            if(typeof window.Hammer !== undefined && device !== undefined && device.type){
                var touchControl = new Hammer($ul.get(0), {domEvents: true});
                if(!vertical){
                    touchControl.on('swipeleft', function() {
                        fun.btnClick(true);
                    });
                    touchControl.on('swiperight', function() {
                        fun.btnClick(false);
                    });
                }else{
                    touchControl.on('swipeup', function() {
                        fun.btnClick(true);
                    });
                    touchControl.on('swipedown', function() {
                        fun.btnClick(false);
                    });
                }
            }

            console.log("-- carousel init: ", this);
            for (var x in o) {
                console.log("[carousel] " + x + ": " + o[x]);
            }

        });
    };
})(jQuery);

// automatically find and run carousel
$(function() {
    $(".carousel").carousel();
    $.event.trigger({
        type: "carouselInit"
    });
});